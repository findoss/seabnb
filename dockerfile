ARG NODE_VERSION=12.10.0
ARG DUMB_INIT_VERSION=1.2.2

# build client
FROM node:${NODE_VERSION}-alpine AS client-build
WORKDIR /home/client
ADD client /home/client
RUN yarn install && yarn build && yarn cache clean

# build server
FROM node:${NODE_VERSION}-alpine AS server-build
ARG DUMB_INIT_VERSION
WORKDIR /home/server
RUN apk add --no-cache build-base python2 yarn && \
    wget -O dumb-init -q https://github.com/Yelp/dumb-init/releases/download/v${DUMB_INIT_VERSION}/dumb-init_${DUMB_INIT_VERSION}_amd64 && \
    chmod +x dumb-init
ADD server /home/server
COPY --from=client-build /home/client/build /home/server/client/build
RUN yarn install && yarn build && yarn cache clean

# Runtime container
FROM node:${NODE_VERSION}-alpine
WORKDIR /home/server
COPY --from=server-build /home/server /home/server
EXPOSE 3000
EXPOSE 465
CMD ["./dumb-init", "yarn", "start"]