import React, { useState } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { AppContext } from "./plugins/context";
import "./App.scss";

import { Menu } from "./components/Menu";
import { Footer } from "./components/Footer";

import { Home } from "./views/Home";
import { ErrorPage } from "./views/ErrorPage";
import { Account } from "./views/Account";
import { Login } from "./views/Login";
import { Registration } from "./views/Registration";
import { ResetPass } from "./views/ResetPass";
import { Rooms } from "./views/Rooms";
import { Room } from "./views/Room";
import { Services } from "./views/Services";
import { Service } from "./views/Service";
import { About } from "./views/About";
import { Reservation } from "./views/Reservation";

export const App = () => {
  const [state, setState] = useState({
    user: {
      isAuth: !!localStorage.getItem("token") || false,
      id: localStorage.getItem("userId") || "",
      name: localStorage.getItem("userName") || "",
      email: localStorage.getItem("userEmail") || "",
    },
  });
  const store = { state, setState };

  return (
    <div className="app">
      <AppContext.Provider value={store}>
        <Menu className="app__menu" />
        <div className="app__page">
          <Switch>
            <Route exact from="/" component={Home} />
            <Redirect exact from="/home" to="/" />
            <Route exact from="/rooms" component={Rooms} />
            <Route exact from="/room/:roomName" component={Room} />
            <Route exact from="/services" component={Services} />
            <Route exact from="/service/:serviceName" component={Service} />
            <Route exact from="/about" component={About} />
            <Route exact from="/account">
              <AppContext.Consumer>
                {(store: any) =>
                  store.state.user.isAuth ? (
                    <Route exact from="/account">
                      <Account store={store} />
                    </Route>
                  ) : (
                    <Redirect exact to={`/account/login`} />
                  )
                }
              </AppContext.Consumer>
            </Route>
            <Route exact from="/account/reg" component={Registration} />
            <Route exact from="/account/login">
              <AppContext.Consumer>
                {(store: any) => <Login store={store} />}
              </AppContext.Consumer>
            </Route>
            <Route exact from="/account/reset" component={ResetPass} />
            <Route exact from="/reservation">
              <AppContext.Consumer>
                {(store: any) => <Reservation store={store} />}
              </AppContext.Consumer>
            </Route>
            <Route component={ErrorPage} />
          </Switch>
        </div>
        <Footer className="app__footer" />
      </AppContext.Provider>
    </div>
  );
};
