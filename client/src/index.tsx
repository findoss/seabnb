import React, { StrictMode, Suspense } from "react";
import ReactDOM from "react-dom";
import { HashRouter as Router } from "react-router-dom";
import { ApolloProvider } from "@apollo/react-hooks";
import { ScrollToTop } from "./components/UI/ScrollToTop";
import "./styles/index.css";
import "./styles/typography.css";

import "./plugins/i18n";
import { clientApollo } from "./plugins/apollo";

import { App } from "./App";
import * as serviceWorker from "./serviceWorker";

ReactDOM.render(
  <StrictMode>
    <Router>
      <ScrollToTop />
      <Suspense fallback="loading">
        <ApolloProvider client={clientApollo}>
          <App />
        </ApolloProvider>
      </Suspense>
    </Router>
  </StrictMode>,
  document.getElementById("root")
);

serviceWorker.unregister();
