import { gql } from "apollo-boost";

export const MUTATION_LOGIN = gql`
  mutation login($email: String, $password: String) {
    authenticateUserWithPassword(email: $email, password: $password) {
      token
      item {
        email
        name
        id
      }
    }
  }
`;

export const loginLogic = (res) => {
  const { token } = res.data.authenticateUserWithPassword;
  const user = res.data.authenticateUserWithPassword.item;

  localStorage.setItem("token", token);
  localStorage.setItem("userId", user.id);
  localStorage.setItem("userName", user.name);
  localStorage.setItem("userEmail", user.email);

  return {
    isAuth: !!token,
    ...user,
  };
};
