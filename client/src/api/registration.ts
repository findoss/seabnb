import { gql } from "apollo-boost";

export const MUTATION_REGISTRATION = gql`
  mutation Registration($email: String, $name: String, $password: String) {
    createUser(data: { email: $email, name: $name, password: $password }) {
      id
    }
  }
`;
