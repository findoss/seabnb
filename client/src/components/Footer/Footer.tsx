import React from "react";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { LINK_MAP } from "../../constants";
import "./Footer.scss";

import { Title } from "../../components/UI/Title";

import { ReactComponent as IconTwitter } from "../../assets/icons/social/twitter.svg";
import { ReactComponent as IconFacebook } from "../../assets/icons/social/facebook.svg";
import { ReactComponent as IconSkype } from "../../assets/icons/social/skype.svg";
import { ReactComponent as IconInstagram } from "../../assets/icons/social/instagram.svg";
import { ReactComponent as IconMail } from "../../assets/icons/mail.svg";
import { ReactComponent as IconPhone } from "../../assets/icons/phone.svg";
import { ReactComponent as IconPlace } from "../../assets/icons/place.svg";

import { Props } from "./types";

import { DATA_INFO, DATA_FOOTER_LINKS } from "../../constants";

export const Footer = ({ className }: Props) => {
  const { t } = useTranslation();
  return (
    <div className={`${className} footer`}>
      <div className="info">
        <Title className="info__title" title={t("info")} />
        <div className="info__text">
          <a href={LINK_MAP} target="_blank" rel="noopener noreferrer">
            <IconPlace className="info__mini-icon" /> {DATA_INFO.address}
          </a>
          <a href={`tel:${DATA_INFO.numberPhone}`}>
            <IconPhone className="info__mini-icon" />
            {DATA_INFO.numberPhoneFormat}
          </a>
          <a href={`mailto:${DATA_INFO.email}`}>
            <IconMail className="info__mini-icon" /> {DATA_INFO.email}
          </a>
        </div>
        <div className="info__social-icons">
          <Link to="#" target="_blank" rel="noopener noreferrer">
            <IconTwitter className="info__icon" />
          </Link>
          <Link to="#" target="_blank" rel="noopener noreferrer">
            <IconFacebook className="info__icon" />
          </Link>
          <Link to="#" target="_blank" rel="noopener noreferrer">
            <IconSkype className="info__icon" />
          </Link>
          <Link to="#" target="_blank" rel="noopener noreferrer">
            <IconInstagram className="info__icon" />
          </Link>
        </div>
      </div>

      <div className="map">
        <Title className="map__title" title={t("map")} />
        <a
          className="map__img"
          href={LINK_MAP}
          target="_blank"
          rel="noopener noreferrer"
        >
          {t("map")}
        </a>
      </div>

      <div className="links">
        <Title className="map__title" title={t("links")} />
        <div className="links__links text_font--2">
          {DATA_FOOTER_LINKS.map((link: any) => {
            return (
              <Link key={link.name} to={`${link.link}`}>
                {t(link.name)}
              </Link>
            );
          })}
        </div>
      </div>
    </div>
  );
};
