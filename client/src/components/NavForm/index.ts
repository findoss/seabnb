import { memo } from "react";
import { NavForm as NavFormComponent } from "./NavForm";

export const NavForm = memo(NavFormComponent);
