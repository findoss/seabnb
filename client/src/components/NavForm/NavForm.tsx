import React from "react";
import { NavLink } from "react-router-dom";
import { useTranslation } from "react-i18next";
import "./NavForm.scss";

import { Props } from "./types";
import { Title } from "../UI/Title";

export const NavForm = ({ className }: Props) => {
  const { t } = useTranslation();
  return (
    <div className={`${className} nav-form`}>
      <NavLink activeClassName="nav-form--active" to={`/account/login`}>
        <Title title={t("login")} />
      </NavLink>
      <NavLink activeClassName="nav-form--active" to={`/account/reg`}>
        <Title title={t("registration")} />
      </NavLink>
    </div>
  );
};
