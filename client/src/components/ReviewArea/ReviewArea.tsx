import React from "react";

import "./ReviewArea.scss";

import { Props } from "./types";

export const ReviewArea = ({ className, ...props }: Props) => {
  console.log(props);

  return <textarea className={`${className} `}>{props.children}</textarea>;
};
