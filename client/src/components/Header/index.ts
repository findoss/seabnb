import { memo } from "react";
import { Header as HeaderComponent } from "./Header";

export const Header = memo(HeaderComponent);
