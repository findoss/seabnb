import React from "react";
import { Link } from "react-router-dom";
import "./Header.scss";

import { Props } from "./types";

export const Header = ({ className, bg }: Props) => {
  return (
    <div className={`${className} header`}>
      <img className="header__bg" src={bg} />
      0000
    </div>
  );
};
