import React, { useState } from "react";
import { Link, NavLink } from "react-router-dom";
import { useTranslation } from "react-i18next";

import "./Menu.scss";

import { Props } from "./types";

import { DATA_MENU, LANGS } from "../../constants";
import { ReactComponent as IconMenu } from "../../assets/icons/menu.svg";
import { ReactComponent as Logo } from "../../assets/images/logo.svg";

export const Menu = ({ className }: Props) => {
  const { t, i18n } = useTranslation();
  const [isOpenMenu, setIsOpenMenu] = useState(false);

  return (
    <div className={`${className} menu`}>
      <Link className="menu__logo logo" to="/">
        <Logo className="logo__img" />
        <div className="logo__text text_font--1">{t("titleHotel")}</div>
      </Link>

      <div className="menu__nav text_font--2">
        <div
          className="menu__nav-burger"
          onClick={(e) => {
            e.preventDefault();
            setIsOpenMenu(!isOpenMenu);
          }}
        >
          <IconMenu />
        </div>

        <div
          className={`menu__nav-container 
          ${isOpenMenu ? "menu__nav-container--open" : ""}`}
        >
          {DATA_MENU.map((link: string, i: number) => {
            return (
              <NavLink
                activeClassName="menu__nav--active"
                className="link"
                onClick={(e) => {
                  setIsOpenMenu(!isOpenMenu);
                }}
                to={`/${link.toLowerCase()}`}
                key={i.toString()}
              >
                {t(link)}
              </NavLink>
            );
          })}
        </div>
      </div>

      <div className="menu__lang">
        <select
          defaultValue={i18n.language}
          onChange={({ target }) => {
            i18n.changeLanguage(target.value);
          }}
        >
          {LANGS.map((lang: string) => {
            return <option key={lang}>{lang}</option>;
          })}
        </select>
      </div>
    </div>
  );
};
