interface OwnProps {
  className?: string;
}

export type Props = OwnProps;
