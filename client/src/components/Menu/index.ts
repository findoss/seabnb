import { memo } from "react";
import { Menu as MenuComponent } from "./Menu";

export const Menu = memo(MenuComponent);
