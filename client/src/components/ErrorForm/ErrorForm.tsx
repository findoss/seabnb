import React, { Fragment } from "react";
import { useTranslation } from "react-i18next";
import "./ErrorForm.scss";

import { Props } from "./types";

import { ReactComponent as IconError } from "../../assets/icons/error.svg";

export const ErrorForm = ({ className, F, L, E }: Props) => {
  const { t } = useTranslation();
  return (
    <Fragment>
      {F || E ? (
        <div className={`${className} error-wrapper animation-shake`}>
          <div className="error">
            <IconError className="icon-error" />
            {F ? (
              <p>{F}</p>
            ) : (
              <Fragment>
                {L && <p>{t("loading")}</p>}
                {E && <p>{t(E)}</p>}
              </Fragment>
            )}
          </div>
        </div>
      ) : null}
    </Fragment>
  );
};
