import { memo } from "react";
import { ErrorForm as ErrorFormComponent } from "./ErrorForm";

export const ErrorForm = memo(ErrorFormComponent);
