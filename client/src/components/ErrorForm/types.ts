interface OwnProps {
  className?: string;
  bg?: string;
}

export type Props = OwnProps & any;
