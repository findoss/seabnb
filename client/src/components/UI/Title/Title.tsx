import React from "react";

import "./Title.scss";

import { Props } from "./types";

export const Title = ({ title, subtitle, className, ...props }: Props) => {
  return (
    <div className={`${className} title-wrapper text_font--1`}>
      <div className="subtitle">{subtitle}</div>
      <div className={`${subtitle ? "title--up" : ""} title`}>{title}</div>
    </div>
  );
};
