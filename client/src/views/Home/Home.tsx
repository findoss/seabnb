import React from "react";
import { useTranslation } from "react-i18next";
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import { Link } from "react-router-dom";
import { Carousel } from "react-responsive-carousel";
import { getPrice } from "../../utils/getPrice";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import "./Home.scss";

import { Title } from "../../components/UI/Title";

const QUERY = gql`
  query {
    homeBlock: allBlocks(where: { name: "home" }, first: 1) {
      name
      photo(first: 3) {
        id
        name
        file {
          id
          publicUrl
        }
      }
      ru_content {
        title
        subtitle
        description
      }
      en_content {
        title
        subtitle
        description
      }
    }
    awardsBlock: allBlocks(where: { name: "awards" }, first: 1) {
      name
      photo(first: 4) {
        name
        file {
          id
          publicUrl
        }
      }
    }
    allRooms(first: 4) {
      id
      name
      photo(first: 1) {
        name
        file {
          publicUrl
        }
      }
      price
      ru_content {
        title
        subtitle
        shortDescription
        description
      }
      en_content {
        title
        subtitle
        shortDescription
        description
      }
    }
    allReviews(first: 4, sortBy: date_DESC) {
      id
      avatar
      user {
        name
      }
      text
    }
    allServices(first: 4) {
      id
      name
      photo(first: 1) {
        name
        file {
          publicUrl
        }
      }
      en_content {
        title
      }
      ru_content {
        title
      }
    }
  }
`;

export const Home = () => {
  const { t, i18n } = useTranslation();
  const { data } = useQuery(QUERY);

  if (data) {
    const { homeBlock, awardsBlock, allRooms, allServices, allReviews } = data;
    const home = homeBlock[0];
    const awards = awardsBlock[0];
    const homeContent = home[`${i18n.language}_content`];

    return (
      <div className="home-page">
        <div className="header">
          <div className="header__text text_font--1">
            {t("enjoyExperience")}
            <br />
            {t("joinWithUs")}
          </div>
          <Link className="header__button" to="/rooms">
            {t("roomAndSuites")}
          </Link>
        </div>
        <br />
        <Title
          className="welcome-block__title"
          title={homeContent.title}
          subtitle={homeContent.subtitle}
        />{" "}
        <div className="welcome-block">
          <div className="welcome-block__des text_font--2">
            {homeContent.description}
          </div>
        </div>
        <div className="welcome-block__img">
          {home.photo.map((img: any) => {
            return (
              <div key={img.id} className="img-wrapper">
                <img
                  src={img.file.publicUrl}
                  key={img.file.id}
                  alt={img.name}
                />
              </div>
            );
          })}
        </div>
        <Title
          className="rooms-block__title"
          title={t("rooms")}
          subtitle={t("luxury")}
        />
        <div className="rooms-block">
          <Carousel
            showArrows={true}
            showThumbs={false}
            infiniteLoop={true}
            autoPlay={true}
            dynamicHeight={false}
            interval={5000}
          >
            {allRooms.map((room: any) => {
              const roomContent = room[`${i18n.language}_content`];
              return (
                <div key={room.id} className="slide-room">
                  <img
                    src={room.photo[0].file.publicUrl}
                    alt={room.photo[0].name}
                    className="slide-room__img"
                  />
                  <div className="slide-room__content">
                    <div className="slide-room__title text_font--1">
                      {roomContent.title}
                    </div>
                    <div className="slide-room__short-des text_font--1">
                      {roomContent.shortDescription}
                    </div>
                    <div className="slide-room__des text_font--2">
                      {roomContent.description}
                    </div>
                    <br />
                    <br />
                    <div className="slide-room__price-wrapper">
                      <div className="slide-room__price text_font--2">
                        {t("$")} {getPrice(room.price, i18n.language)} /{" "}
                        {t("night")}
                      </div>
                      <Link to={`/room/${room.name}`}>{t("learnMore")}</Link>
                    </div>
                  </div>
                </div>
              );
            })}
          </Carousel>
        </div>
        <Title title={t("services")} subtitle={t("awesome")} />
        <div className="service-block">
          {allServices.map((service: any) => {
            return (
              <Link
                to={`/service/${service.name}`}
                className="service-block__item"
                key={service.id}
              >
                <div className="service-block__text">
                  {service[`${i18n.language}_content`].title}
                </div>
                <div className="img-wrapper">
                  <img
                    src={service.photo[0].file.publicUrl}
                    alt="service"
                    className="service-block__img"
                  />
                </div>
              </Link>
            );
          })}
        </div>
        <Title title={t("testimonials")} subtitle={t("client")} />
        <div className="review-block">
          <Carousel
            showArrows={true}
            showThumbs={false}
            infiniteLoop={true}
            autoPlay={false}
            dynamicHeight={false}
            interval={5000}
            showStatus={false}
            showIndicators={false}
          >
            {allReviews.map((review: any) => {
              return (
                <div className="review" key={review.id}>
                  <img alt="avatar" src={review.avatar} />
                  <div className="review__des">
                    <div className="review__text text_font--2">
                      {review.text}
                    </div>
                    <div className="review__name text_font--2">
                      {review.user.name}
                    </div>
                  </div>
                </div>
              );
            })}
          </Carousel>
        </div>
        {awards ? (
          <div className="award-block">
            {awards.photo.map((award: any) => {
              return (
                <img
                  className="award__img"
                  key={award.file.id}
                  alt={award.name}
                  src={award.file.publicUrl}
                />
              );
            })}
          </div>
        ) : (
          ""
        )}
      </div>
    );
  }
  return null;
};
