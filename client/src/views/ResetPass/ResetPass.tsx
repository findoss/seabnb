import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { gql } from "apollo-boost";
import { validateEmail } from "../../utils/validationEmail";
import { useQuery, useMutation } from "@apollo/react-hooks";
import "./ResetPass.scss";

import { Button } from "../../components/UI/Button";
import { Title } from "../../components/UI/Title";
import { TextField } from "../../components/UI/TextField";
import { NavForm } from "../../components/NavForm";
import { ErrorForm } from "../../components/ErrorForm";

const MUTATION_RESET_PASS = gql`
  mutation ResetPass($id: ID!) {
    updateUser(id: $id, data: { password: "reset_password" }) {
      id
    }
  }
`;

const QUERY_ID_BY_EMAIL = gql`
  query($email: String) {
    allUsers(where: { email: $email }) {
      id
    }
  }
`;

export const ResetPass = () => {
  let history = useHistory();
  const { t } = useTranslation();

  const [
    resetPass,
    { loading: resetPassLoading, error: resetPassError },
  ] = useMutation(MUTATION_RESET_PASS);

  const [resetPassErrorFields, setresetPassError] = useState("");
  const [email, setEmail] = useState("");
  const { data } = useQuery(QUERY_ID_BY_EMAIL, {
    variables: { email: email },
  });

  return (
    <div className="reset-page">
      <br />
      <NavForm />
      <br />

      <div className="reset-page__form">
        <ErrorForm
          className="login-page__form-error"
          F={resetPassErrorFields}
          L={resetPassLoading}
          E={resetPassError && t("error.resetPass")}
        />
        <br />
        <form
          className="form"
          onSubmit={async (e: React.FormEvent) => {
            e.preventDefault();
            if (!email) {
              setresetPassError(t("error.email.require"));
              return;
            } else if (!validateEmail(email)) {
              setresetPassError(t("error.email.format"));
              return;
            } else if (!data.allUsers.length) {
              setresetPassError(t("error.email.404"));
              return;
            }

            try {
              await resetPass({
                variables: {
                  id: data.allUsers[0].id,
                },
              });
              setEmail("");

              history.push("/account/login");
            } catch (error) {
              console.info(error);
            }
          }}
        >
          <Title title={t("resetPass")} />
          <br />
          <label>{t("email")}</label>
          <TextField
            tabIndex="1"
            className="form__field"
            type="text"
            name="email"
            placeholder={t("emailPlaceholder")}
            value={email}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
              setEmail(e.target.value);
            }}
          />
          <Button tabIndex="2" type="submit">
            {t("resetPass")}
          </Button>
        </form>
      </div>
    </div>
  );
};
