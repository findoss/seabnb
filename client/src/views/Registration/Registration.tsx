import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { validateEmail } from "../../utils/validationEmail";
import { useMutation } from "@apollo/react-hooks";
import { AppContext } from "../../plugins/context";
import "./Registration.scss";

import { Title } from "../../components/UI/Title";
import { Button } from "../../components/UI/Button";
import { TextField } from "../../components/UI/TextField";
import { NavForm } from "../../components/NavForm";
import { ErrorForm } from "../../components/ErrorForm";
import { MUTATION_LOGIN, loginLogic } from "../../api/login";
import { MUTATION_REGISTRATION } from "../../api/registration";

export const Registration = () => {
  let history = useHistory();
  const { t } = useTranslation();

  const [registration, { loading: regLoading, error: regError }] = useMutation(
    MUTATION_REGISTRATION
  );
  const [login] = useMutation(MUTATION_LOGIN);

  const [regErrorFields, setRegError] = useState("");

  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");

  return (
    <div className="registration-page">
      <br />
      <NavForm />
      <br />

      <div className="registration-page__form form">
        <ErrorForm
          className="login-page__form-error"
          F={regErrorFields}
          L={regLoading}
          E={regError && t("error.reg")}
        />
        <AppContext.Consumer>
          {({ state, setState }) => {
            return (
              <form
                className="form"
                onSubmit={async (e: React.FormEvent) => {
                  e.preventDefault();
                  if (!email) {
                    setRegError(t("error.email.require"));
                    return;
                  } else if (!validateEmail(email)) {
                    setRegError(t("error.email.format"));
                    return;
                  } else if (!name) {
                    setRegError(t("error.name.require"));
                    return;
                  } else if (!password) {
                    setRegError(t("error.password.require"));
                    return;
                  } else if (password.length < 8) {
                    setRegError(t("error.password.format"));
                    return;
                  }

                  try {
                    await registration({
                      variables: {
                        email: email,
                        name: name,
                        password: password,
                      },
                    });

                    const res = await login({
                      variables: { email: email, password: password },
                    });

                    const userState = loginLogic(res);
                    setState({ user: userState });

                    setEmail("");
                    setName("");
                    setPassword("");
                    history.push("/account");
                  } catch (error) {
                    console.info(error);
                  }
                }}
              >
                <Title title={t("registration")} />
                <label>{t("name")}</label>
                <TextField
                  tabIndex="1"
                  className="form__field"
                  type="text"
                  name="name"
                  placeholder={t("namePlaceholder")}
                  value={name}
                  onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                    setName(e.target.value);
                  }}
                />
                <label>{t("email")}</label>
                <TextField
                  tabIndex="2"
                  className="form__field"
                  type="text"
                  name="email"
                  placeholder={t("emailPlaceholder")}
                  value={email}
                  onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                    setEmail(e.target.value);
                  }}
                />
                <label>{t("password")}</label>
                <TextField
                  tabIndex="3"
                  className="form__field"
                  type="password"
                  name="password"
                  placeholder={t("passwordPlaceholder")}
                  value={password}
                  onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                    setPassword(e.target.value);
                  }}
                />
                <Button tabIndex="4" type="submit">
                  {t("registration")}
                </Button>
              </form>
            );
          }}
        </AppContext.Consumer>
      </div>
    </div>
  );
};
