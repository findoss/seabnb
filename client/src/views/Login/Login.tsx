import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { useMutation } from "@apollo/react-hooks";
import { useTranslation } from "react-i18next";
import { validateEmail } from "../../utils/validationEmail";
import { Props } from "./types";
import "./Login.scss";

import { Title } from "../../components/UI/Title";
import { Button } from "../../components/UI/Button";
import { TextField } from "../../components/UI/TextField";
import { NavForm } from "../../components/NavForm";
import { ErrorForm } from "../../components/ErrorForm";
import { MUTATION_LOGIN, loginLogic } from "../../api/login";

export const Login = ({ store }: Props) => {
  let history = useHistory();
  const { t } = useTranslation();
  const [login, { loading: loginLoading, error: loginError }] = useMutation(
    MUTATION_LOGIN
  );

  const { setState } = store;
  const [email, setEmail] = useState("");
  const [pass, setPass] = useState("");
  const [loginErrorFields, setLoginError] = useState("");

  return (
    <div className="login-page">
      <br />
      <NavForm className="login-page__nav" />

      <div className="login-page__form">
        <ErrorForm
          className="login-page__form-error"
          F={loginErrorFields}
          L={loginLoading}
          E={loginError && t("error.login")}
        />

        <form
          className="form"
          onSubmit={async (e) => {
            e.preventDefault();
            if (!email) {
              setLoginError(t("error.email.require"));
              return;
            } else if (!validateEmail(email)) {
              setLoginError(t("error.email.format"));
              return;
            } else if (!pass) {
              setLoginError(t("error.pass.require"));
              return;
            }

            setLoginError("");
            try {
              const res = await login({
                variables: { email: email, password: pass },
              });

              const userState = loginLogic(res);

              setState({ user: userState });

              setEmail("");
              setPass("");

              history.push("/account");
            } catch (error) {
              setPass("");
              console.error(error.message);
            }
          }}
        >
          <Title title={t("login")} />
          <label>{t("email")}</label>
          <TextField
            tabIndex="1"
            className="form__field"
            type="text"
            name="email"
            placeholder={t("emailPlaceholder")}
            value={email}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
              setEmail(e.target.value);
            }}
          />
          <div>
            <label>{t("password")}</label>
            <Link style={{ float: "right" }} to={`/account/reset`}>
              {t("forgotPassword")}
            </Link>
          </div>
          <TextField
            tabIndex="2"
            className="form__field"
            type="password"
            name="password"
            placeholder={t("passwordPlaceholder")}
            value={pass}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
              setPass(e.target.value);
            }}
          />
          <Button tabIndex="3" type="submit">
            {t("login")}
          </Button>
        </form>
      </div>
    </div>
  );
};
