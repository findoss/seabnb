import React from "react";
import { useTranslation } from "react-i18next";
import { useQuery } from "@apollo/react-hooks";
import { useParams } from "react-router-dom";
import { gql } from "apollo-boost";
import ReactMarkdown from "react-markdown";
import "./Service.scss";

import { Title } from "../../components/UI/Title";

const QUERY = gql`
  query($name: String) {
    allServices(where: { name: $name }) {
      name
      photo(skip: 1) {
        file {
          id
          publicUrl
        }
      }
      en_content {
        title
        subtitle
        description
      }
      ru_content {
        title
        subtitle
        description
      }
    }
  }
`;

export const Service = () => {
  const { i18n } = useTranslation();
  const { serviceName } = useParams();
  const { data } = useQuery(QUERY, {
    variables: { name: serviceName },
  });

  if (data) {
    const { allServices } = data;
    const service = allServices[0];
    const serviceContent = service[`${i18n.language}_content`];

    return (
      <div className="service-page">
        <Title
          title={serviceContent.title}
          subtitle={serviceContent.subtitle}
        />

        <div className="service-page__wrapper">
          <div className="service-page__gal">
            {service.photo.map((img) => {
              return (
                <img
                  className="service-page__img"
                  key={img.file.id}
                  src={img.file.publicUrl}
                  alt="service"
                />
              );
            })}
          </div>
          <div className="service-page__des">
            <ReactMarkdown
              source={serviceContent.description}
              escapeHtml={false}
            />
          </div>
        </div>
      </div>
    );
  }
  return null;
};
