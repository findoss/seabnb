import React, { Fragment } from "react";
import { useTranslation } from "react-i18next";
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import ReactMarkdown from "react-markdown";
import "./About.scss";

import { Title } from "../../components/UI/Title";

const QUERY = gql`
  query {
    aboutBlock: allBlocks(where: { name: "about" }, first: 1) {
      name
      en_content {
        title
        subtitle
        description
      }
      ru_content {
        title
        subtitle
        description
      }
      photo {
        name
        file {
          id
          publicUrl
        }
      }
    }
    allHistories {
      name
      ru_content {
        title
        description
      }
      en_content {
        title
        description
      }
    }
  }
`;

export const About = () => {
  const { t, i18n } = useTranslation();
  const { data } = useQuery(QUERY);

  if (data) {
    const { aboutBlock, allHistories } = data;
    const about = aboutBlock[0];
    const aboutContent = about[`${i18n.language}_content`];

    return (
      <div className="about-page">
        <Title title={aboutContent.title} subtitle={aboutContent.subtitle} />
        <div className="about-page__place">
          <div className="about-page__des text_font--2">
            <ReactMarkdown
              source={aboutContent.description}
              escapeHtml={false}
            />
          </div>
          <img
            src={about.photo[0].file.publicUrl}
            alt={about.photo[0].file.id}
            className="about-page__img"
          />
        </div>
        <br />
        <br />
        <div className="about-page__history history">
          <Title title={t("history")} subtitle={t("story")} />
          <img
            src={about.photo[1].file.publicUrl}
            alt={about.photo[1].file.id}
            className="history__img"
          />
          <div className="history__wrapper">
            {allHistories.map((history) => {
              const historyContent = history[`${i18n.language}_content`];
              return (
                <Fragment key={history.name}>
                  <div className="history__name text_font--1">
                    {history.name}
                  </div>
                  <div className="history__title text_font--1">
                    {historyContent.title}
                  </div>
                  <div className="history__des text_font--2">
                    {historyContent.description}
                  </div>
                </Fragment>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
  return null;
};
