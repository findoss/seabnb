import React from "react";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";

import "./ErrorPage.scss";

import { ReactComponent as IconError } from "../../assets/icons/error.svg";

export const ErrorPage = () => {
  const { t } = useTranslation();
  return (
    <div className="error-page">
      <IconError className="error-page__icon" />
      <span className="error-page__text">{t("page404")}</span>
      <Link to="/" className="error-page__link">
        {t("goHome")}
      </Link>
    </div>
  );
};
