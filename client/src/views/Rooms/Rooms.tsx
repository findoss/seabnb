import React from "react";
import { useTranslation } from "react-i18next";
import { useQuery } from "@apollo/react-hooks";
import { Link } from "react-router-dom";
import { gql } from "apollo-boost";
import { getPrice } from "../../utils/getPrice";
import "./Rooms.scss";

import { Title } from "../../components/UI/Title";
import { Button } from "../../components/UI/Button";

const QUERY = gql`
  query {
    allRooms(first: 20) {
      id
      price
      name
      en_content {
        title
      }
      ru_content {
        title
      }
      photo(first: 1) {
        file {
          id
          publicUrl
        }
      }
    }
    allReviews(first: 30, sortBy: date_DESC) {
      id
      avatar
      date
      text
      user {
        name
      }
    }
    awardsBlock: allBlocks(where: { name: "awards" }, first: 1) {
      name
      en_content {
        title
        subtitle
        description
      }
      ru_content {
        title
        subtitle
        description
      }
      photo {
        name
        file {
          id
          publicUrl
        }
      }
    }
  }
`;

export const Rooms = () => {
  const { t, i18n } = useTranslation();
  const { data } = useQuery(QUERY);

  if (data) {
    const { allRooms, allReviews, awardsBlock } = data;
    const awards = awardsBlock[0];
    const awardsContent = awards[`${i18n.language}_content`];

    return (
      <div className="rooms-page">
        <Title title={`${t("rooms")}`} subtitle={t("awesome")} />
        <div className="room">
          {allRooms.map((room) => {
            const roomContent = room[`${i18n.language}_content`];
            return (
              <Link
                className="room-item"
                key={room.id}
                to={`/room/${room.name}`}
              >
                <div className="img-wrapper">
                  <img
                    className="room-item__img"
                    src={room.photo[0].file.publicUrl}
                    alt={room.photo[0].file.id}
                  />
                </div>
                <div className="room-item__title text_font--1">
                  {roomContent.title}
                </div>
                <div className="room-item__price">
                  {t("$")} {getPrice(room.price, i18n.language)} / {t("night")}
                </div>
              </Link>
            );
          })}
        </div>
        <Button>
          <Link to="/reservation">{t("goReservation")}</Link>
        </Button>
        <Title title={t("reviews")} subtitle={t("bright")} />
        <div className="review-block">
          {allReviews.map((review) => {
            return (
              <div key={review.id} className="review-block__item">
                <img
                  className="review-block__img"
                  src={review.avatar}
                  alt="avatar"
                />
                <div className="review-block__content">
                  <div className="review-block__top">
                    <div className="review-block__name">{review.user.name}</div>
                    <div className="review-block__date">
                      {new Date(review.date).toLocaleDateString(i18n.language)}
                    </div>
                  </div>
                  <div className="review-block__text">{review.text}</div>
                </div>
              </div>
            );
          })}
        </div>

        <Title title={t("ourAwards")} subtitle={t("winner")} />
        <div className="award-block">
          <div className="award-block__right">
            <div className="award-block__tittle text_font--1">
              {awardsContent.title}
            </div>
            <div className="award-block__subtitle text_font--1">
              {awardsContent.subtitle}
            </div>
            <div className="award-block__des text_font--2">
              {awardsContent.description}
            </div>
          </div>
          <div className="award-block__img">
            {awards.photo.map((photo) => {
              return (
                <img
                  key={photo.file.id}
                  src={photo.file.publicUrl}
                  alt="award"
                />
              );
            })}
          </div>
        </div>
      </div>
    );
  }
  return null;
};
