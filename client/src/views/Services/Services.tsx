import React from "react";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import "./Services.scss";

import { Title } from "../../components/UI/Title";

const QUERY = gql`
  query {
    allServices {
      id
      name
      photo(first: 1) {
        file {
          id
          publicUrl
        }
      }
      en_content {
        title
      }
      ru_content {
        title
      }
    }
    serviceBlock: allBlocks(where: { name: "service" }, first: 1) {
      name
      en_content {
        title
        subtitle
        description
      }
      ru_content {
        title
        subtitle
        description
      }
      photo {
        name
        file {
          id
          publicUrl
        }
      }
    }
  }
`;

export const Services = () => {
  const { i18n } = useTranslation();
  const { data } = useQuery(QUERY);

  if (data) {
    const { serviceBlock, allServices } = data;
    const serviceContent = serviceBlock[0][`${i18n.language}_content`];

    return (
      <div className="services-page">
        <Title
          title={serviceContent.title}
          subtitle={serviceContent.subtitle}
        />
        <div className="services-page__des">{serviceContent.description}</div>
        <div className="service-gall">
          {allServices.map((service: any) => {
            return (
              <Link
                to={`/service/${service.name}`}
                className="service-gall__item"
                key={service.id}
              >
                <div className="service-gall__text">
                  {service[`${i18n.language}_content`].title}
                </div>
                <div className="img-wrapper">
                  <img
                    src={service.photo[0].file.publicUrl}
                    alt="service"
                    className="service-gall__img"
                  />
                </div>
              </Link>
            );
          })}
        </div>
      </div>
    );
  }
  return null;
};
