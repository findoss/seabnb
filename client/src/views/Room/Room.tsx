import React, { Fragment } from "react";
import { gql } from "apollo-boost";
import { useQuery } from "@apollo/react-hooks";
import { useTranslation } from "react-i18next";
import { Link, useParams } from "react-router-dom";
import "./Room.scss";

import ReactMarkdown from "react-markdown";
import { Carousel } from "react-responsive-carousel";
import { Title } from "../../components/UI/Title";
import { Button } from "../../components/UI/Button";
import { getPrice } from "../../utils/getPrice";

import { ReactComponent as IconMiniBar } from "../../assets/icons/details/bar.svg";
import { ReactComponent as IconConditioner } from "../../assets/icons/details/air-conditioner.svg";
import { ReactComponent as IconTV } from "../../assets/icons/details/tv.svg";
import { ReactComponent as IconRoomService } from "../../assets/icons/details/room-service.svg";
import { ReactComponent as IconHairDryer } from "../../assets/icons/details/hair-dryer.svg";
import { ReactComponent as IconBathtub } from "../../assets/icons/details/bathtub.svg";

const QUERY = gql`
  query($name: String) {
    allRooms(where: { name: $name }) {
      id
      name
      type
      price
      count
      detal {
        bed
        occupancy
        size
        wifi
        bathroom
        isBreakfast
        gymAccess
        airportPickup
        roomSevice
        view
      }
      photo {
        file {
          id
          publicUrl
        }
      }
      en_content {
        title
        subtitle
        description
        shortDescription
      }
      ru_content {
        title
        subtitle
        description
        shortDescription
      }
    }
    roomLinks: allRooms {
      id
      name
      en_content {
        title
      }
      ru_content {
        title
      }
    }
  }
`;

const icons = [
  {
    icon: <IconMiniBar />,
    title: "minibar",
  },
  {
    icon: <IconConditioner />,
    title: "conditioner",
  },
  {
    icon: <IconTV />,
    title: "tv",
  },
  {
    icon: <IconRoomService />,
    title: "roomService",
  },
  {
    icon: <IconHairDryer />,
    title: "hairDryer",
  },
  {
    icon: <IconBathtub />,
    title: "bathroom",
  },
];

export const Room = () => {
  const { t, i18n } = useTranslation();
  const { roomName } = useParams();
  const { data } = useQuery(QUERY, {
    variables: { name: roomName },
  });

  if (data) {
    const { roomLinks, allRooms } = data;
    const room = allRooms[0];
    const roomContent = room[`${i18n.language}_content`];

    return (
      <div className="room-page">
        <Title title={roomContent.title} subtitle={roomContent.subtitle} />
        <div className="room-page__gal">
          <div className="room-page__header">
            <div className="room-page__short-des text_font--1">
              {roomContent.shortDescription}
            </div>
            <div className="room-page__price text_font--2">
              {t("$")} {getPrice(room.price, i18n.language)} / {t("night")}
            </div>
          </div>
          <Carousel
            showArrows={true}
            showThumbs={false}
            infiniteLoop={true}
            autoPlay={true}
            dynamicHeight={true}
            interval={5000}
          >
            {room.photo.map((img) => {
              return (
                <img
                  src={img.file.publicUrl}
                  alt="room"
                  key={img.file.id}
                  width="200px"
                />
              );
            })}
          </Carousel>
        </div>

        <Button>
          <Link to="/reservation">{t("goReservation")}</Link>
        </Button>
        <div className="room-icons">
          {icons.map((icon) => {
            return (
              <div className="room-icons__icon-wrapper" key={icon.title}>
                <div className="room-icons__icon">{icon.icon}</div>
                <div className="room-icons__title">{t(icon.title)}</div>
              </div>
            );
          })}
        </div>

        <Title title={t("description")} subtitle={t("greatThing")} />
        <ReactMarkdown
          className="room__des text_font--2"
          source={roomContent.description}
          escapeHtml={false}
        />
        <br />
        <Title title={t("details")} />
        <div className="room__list room-list">
          {Object.keys(room.detal).map((f) => {
            const v = room.detal[f];
            if (f !== "__typename")
              return (
                <div
                  className="room-list__item text_font--2"
                  key={f.toString()}
                >
                  <div>{t(f)}</div>
                  <div>{t(v)}</div>
                </div>
              );
            return null;
          })}
        </div>
        <Button>
          <Link to="/reservation">{t("goReservation")}</Link>
        </Button>
        <br />
        <Title title={t("rooms")} subtitle={t("other")} />
        <div className="room-links">
          {(() => {
            const index = roomLinks.findIndex(({ name }) => name === roomName);
            const preLink = roomLinks[index - 1];
            const nextLink = roomLinks[index + 1];
            return (
              <Fragment>
                {preLink ? (
                  <div>
                    <span className="room-links__tip">
                      {t("pre")} {t("room")}
                    </span>
                    <br />
                    <Link
                      className="room-links__link"
                      to={`/room/${preLink.name}`}
                    >
                      {preLink[`${i18n.language}_content`].title}
                    </Link>
                  </div>
                ) : (
                  <div></div>
                )}
                {nextLink && (
                  <div className="room-links--r">
                    <span className="room-links__tip">
                      {t("next")} {t("room")}
                    </span>
                    <br />
                    <Link
                      className="room-links__link"
                      to={`/room/${nextLink.name}`}
                    >
                      {nextLink[`${i18n.language}_content`].title}
                    </Link>
                  </div>
                )}
              </Fragment>
            );
          })()}
        </div>
      </div>
    );
  }
  return null;
};
