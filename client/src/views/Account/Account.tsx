import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { gql } from "apollo-boost";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { Props } from "./types";
import "./Account.scss";

import { Title } from "../../components/UI/Title";
import { Button } from "../../components/UI/Button";
import { ErrorForm } from "../../components/ErrorForm";

const MUTATION_SEND_REVIEW = gql`
  mutation review($text: String) {
    createReview(data: { text: $text }) {
      id
    }
  }
`;

const MUTATION_LOGOUT = gql`
  mutation {
    unauthenticateUser {
      success
    }
  }
`;

const QUERY_RESERVATIONS = gql`
  query getReservation($userId: ID) {
    allReservations(sortBy: id_DESC, where: { user: { id: $userId } }) {
      shortId
      dateIn
      dateOut
      room {
        en_content {
          title
        }
        ru_content {
          title
        }
      }
      customer
      name
      status
    }
  }
`;

export const Account = ({ store }: Props) => {
  let history = useHistory();
  const { t, i18n } = useTranslation();
  const { data } = useQuery(QUERY_RESERVATIONS, {
    variables: { userId: store.state.user.id },
  });

  const [review, { loading: reviewLoading, error: reviewError }] = useMutation(
    MUTATION_SEND_REVIEW
  );

  const [reviwErrorFields, setRevieError] = useState("");
  const [successReview, setSuccessReview] = useState(false);
  const [text, setText] = useState("");
  const [logout] = useMutation(MUTATION_LOGOUT);

  return (
    <div className="account-page">
      <Title title={t("details")} subtitle={t("account")} />
      <br />
      {data ? (
        <div className="table-reservation">
          <table>
            <thead>
              <tr>
                <td>{t("shortId")}</td>
                <td>{t("date")}</td>
                <td>{t("room")}</td>
                <td>{t("customer")}</td>
                <td>{t("name")}</td>
                <td>{t("status")}</td>
              </tr>
            </thead>
            <tbody>
              {data.allReservations.length ? (
                data.allReservations.map((reservation) => {
                  return (
                    <tr key={reservation.shortId}>
                      <td>{reservation.shortId}</td>
                      <td>
                        {new Date(reservation.dateIn).toLocaleDateString(
                          i18n.language
                        )}
                        <br />
                        {new Date(reservation.dateOut).toLocaleDateString(
                          i18n.language
                        )}
                      </td>

                      <td>
                        {reservation.room[`${i18n.language}_content`].title}
                      </td>
                      <td>{reservation.customer}</td>
                      <td>{reservation.name}</td>
                      <td className="table-reservation__status">
                        {t(reservation.status)}
                      </td>
                    </tr>
                  );
                })
              ) : (
                <tr>
                  <td className="table-reservation--empty" colSpan={6}>
                    {t("empty")}
                  </td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
      ) : null}
      <br />

      <Button
        onClick={(e) => {
          logout();
          localStorage.removeItem("token");
          localStorage.removeItem("userId");
          localStorage.removeItem("userName");
          localStorage.removeItem("userEmail");

          store.setState({
            user: {
              id: "",
              isAuth: "",
              email: "",
              name: "",
            },
          });

          history.push("/");
        }}
      >
        {t("logout")}
      </Button>

      <div className="review-block">
        <div className="account-page__title text_font--1">
          {t("writeReview")}
        </div>
        <ErrorForm F={reviwErrorFields} L={reviewLoading} E={reviewError} />
        {successReview && (
          <div className="review-block__success animation-shake">
            {t("successReview")}
          </div>
        )}
        <form
          className="form"
          onSubmit={async (e: React.FormEvent) => {
            e.preventDefault();
            if (!text) {
              setRevieError(t("error.review.require"));
              return;
            }
            try {
              await review({
                variables: { text: text },
              });
              setText("");
              setSuccessReview(true);
            } catch (error) {
              console.info(error);
            }
          }}
        >
          <textarea
            className=""
            value={text}
            rows={5}
            onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) => {
              setText(e.currentTarget.value);
            }}
          />
          <Button type="submit">{t("sendReview")}</Button>
        </form>
      </div>
    </div>
  );
};
