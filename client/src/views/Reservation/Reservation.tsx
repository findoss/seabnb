import React, { Fragment, useState } from "react";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { gql } from "apollo-boost";
import { validateEmail } from "../../utils/validationEmail";
import { useMutation } from "@apollo/react-hooks";
import { useQuery } from "@apollo/react-hooks";
import { Props } from "./types";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import "./Reservation.scss";

import { Title } from "../../components/UI/Title";
import { Button } from "../../components/UI/Button";
import { TextField } from "../../components/UI/TextField";
import { ErrorForm } from "../../components/ErrorForm";

import { MAX_DAY_RESERVATION } from "../../constants";
import { MUTATION_LOGIN, loginLogic } from "../../api/login";
import { MUTATION_REGISTRATION } from "../../api/registration";

const DAY = 1000 * 60 * 60 * 25;

const QUERY = gql`
  query {
    allRooms {
      id
      en_content {
        title
      }
      ru_content {
        title
      }
    }
  }
`;

const MUTATION_RESERVATION = gql`
  mutation(
    $dateIn: String
    $dateOut: String
    $customer: Int
    $room: ID!
    $name: String
  ) {
    createReservation(
      data: {
        dateIn: $dateIn
        dateOut: $dateOut
        customer: $customer
        room: { connect: { id: $room } }
        name: $name
      }
    ) {
      shortId
    }
  }
`;

export const Reservation = ({ store }: Props) => {
  let history = useHistory();
  const { t, i18n } = useTranslation();
  const { data } = useQuery(QUERY);

  const [reservation, { loading: resLoading, error: resError }] = useMutation(
    MUTATION_RESERVATION
  );
  const [registration, { loading: regLoading, error: regError }] = useMutation(
    MUTATION_REGISTRATION
  );
  const [login] = useMutation(MUTATION_LOGIN);

  const { state, setState } = store;
  const [inDate, setInDate] = useState(new Date());
  const [outDate, setOutDate] = useState(new Date(Date.now() + DAY));
  const [email, setEmail] = useState(state.user ? state.user.email : "");
  const [name, setName] = useState(state.user ? state.user.name : "");
  const [customer, setCustomer] = useState(1);
  const [roomId, setRoomId] = useState("");
  const [password, setPassword] = useState("");
  const [resErrorFields, setResErrorFields] = useState("");

  return (
    <div className="reservation-page">
      <Title
        title={t("room") + " " + t("reservation")}
        subtitle={t("reservation")}
      />
      <br />
      <div className="reservation-page__form form">
        <ErrorForm
          className="login-page__form-error"
          F={resErrorFields}
          L={resLoading || regLoading}
          E={(resError || regError) && t("error.res")}
        />
        <form
          className="form"
          onSubmit={async (e: React.FormEvent) => {
            e.preventDefault();
            setResErrorFields("");

            if (!email) {
              setResErrorFields(t("error.email.require"));
              return;
            } else if (!validateEmail(email)) {
              setResErrorFields(t("error.email.format"));
              return;
            } else if (!name) {
              setResErrorFields(t("error.name.require"));
              return;
            } else if (!roomId) {
              setResErrorFields(t("error.room.require"));
              return;
            }

            if (state.user.id) {
              // reservation
              try {
                await reservation({
                  variables: {
                    dateIn: new Date(inDate).toISOString(),
                    dateOut: new Date(outDate).toISOString(),
                    customer: customer,
                    room: roomId,
                    name: name,
                  },
                });

                history.push("/account");
              } catch (error) {
                console.error(error.message);
              }
            } else {
              // registration, reservation
              if (password.length < 8) {
                setResErrorFields(t("error.password.format"));
                return;
              }

              try {
                await registration({
                  variables: {
                    email: email,
                    name: name,
                    password: password,
                  },
                });
              } catch (error) {
                const regexp = new RegExp("E11000", "gi");
                if (!regexp.test(error.message)) {
                  throw new Error(error);
                }
              }

              try {
                const res = await login({
                  variables: { email: email, password: password },
                });

                const userState = loginLogic(res);

                await reservation({
                  variables: {
                    dateIn: new Date(inDate).toISOString(),
                    dateOut: new Date(outDate).toISOString(),
                    customer: customer,
                    room: roomId,
                    name: name,
                  },
                });

                await setState({ user: userState });
                history.push("/account");
              } catch (error) {
                console.error(error.message);

                const regexp = new RegExp(
                  "[passwordAuth:secret:mismatch]",
                  "gi"
                );
                if (regexp.test(error.message)) {
                  setResErrorFields(t("error.login.res"));
                  return;
                }
              }
            }
          }}
        >
          <div className="calendar">
            <div className="calendar__1">
              <label>
                {t("date")} {t("in")}
              </label>
              <DatePicker
                dateFormat="dd.MM.yyyy"
                placeholderText={t("placeholderInDate")}
                className="calendar-input"
                selected={inDate}
                onChange={(date) => {
                  console.log(date);
                  setInDate(date);
                  if (outDate < date) {
                    setOutDate(new Date(Date.parse(String(date)) + DAY));
                  }
                }}
                minDate={new Date()}
                inline
              />
            </div>
            <div className="calendar__2">
              <label>
                {t("date")} {t("out")}
              </label>
              <DatePicker
                dateFormat="dd.MM.yyyy"
                placeholderText={t("placeholderOutDate")}
                className="calendar-input"
                selected={outDate}
                minDate={new Date(Date.parse(String(inDate)) + DAY)}
                maxDate={
                  new Date(
                    Date.parse(String(inDate)) + DAY * MAX_DAY_RESERVATION
                  )
                }
                onChange={(date) => setOutDate(date)}
                inline
              />
            </div>
          </div>
          <br />

          <label>{t("room")}</label>
          {data
            ? (() => {
                const { allRooms } = data;
                return (
                  <select
                    className="form__field select"
                    defaultValue=""
                    onChange={(e: React.ChangeEvent<HTMLSelectElement>) => {
                      setRoomId(e.target.value);
                    }}
                  >
                    <option value="">{t("roomPlaceholder")}</option>
                    {allRooms.map((room) => {
                      return (
                        <option key={room.id} value={room.id}>
                          {room[`${i18n.language}_content`].title}
                        </option>
                      );
                    })}
                  </select>
                );
              })()
            : null}

          <label>{t("customer")}</label>
          <TextField
            tabIndex="3"
            className="form__field"
            type="number"
            min="1"
            max="5"
            name="customer"
            placeholder={t("emailPlaceholder")}
            value={customer}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
              setCustomer(Number(e.target.value));
            }}
          />
          <Title
            title={state.user.id ? t("user") : t("new") + " " + t("user")}
          />
          <br />
          <label>{t("name")}</label>
          <TextField
            tabIndex="1"
            className="form__field"
            type="text"
            name="name"
            placeholder={t("namePlaceholder")}
            value={name}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
              setName(e.target.value);
            }}
          />
          <label>{t("email")}</label>
          <TextField
            tabIndex="2"
            className="form__field"
            type="text"
            name="email"
            disabled={state.user.id}
            placeholder={t("emailPlaceholder")}
            value={email}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
              setEmail(e.target.value);
            }}
          />
          {!state.user.id ? (
            <Fragment>
              <label>{t("password")}</label>
              <TextField
                tabIndex="3"
                className="form__field"
                type="password"
                name="password"
                placeholder={t("passwordPlaceholder")}
                value={password}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                  setPassword(e.target.value);
                }}
              />
            </Fragment>
          ) : null}
          <ErrorForm
            className="login-page__form-error"
            F={resErrorFields}
            L={resLoading || regLoading}
            E={(resError || regError) && t("error.res")}
          />
          <Button className="form__submit" tabIndex="3" type="submit">
            {t("reservation")}
          </Button>
        </form>
      </div>
    </div>
  );
};
