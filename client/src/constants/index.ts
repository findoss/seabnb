export const API_URL: string =
  process.env.NODE_ENV === "development"
    ? "http://localhost:3000/admin/api"
    : "https://sea-bnb.site/admin/api";

export const ONE_DAY: number = 24 * 60 * 60 * 1000;

export const MAX_DAY_RESERVATION: number = 60;

export const LANGS: string[] = ["en", "ru"];

export const EXCHANGE_RATES = {
  en: 1,
  ru: 70,
};

export const LINK_MAP =
  "https://yandex.ru/maps/?um=constructor%3Afd3a41c9d8ba5f3143eed24d9e5fceaa4c97d41e43a14cb2f92d54b85ea34ece&amp;source=constructorStatic";

export const DATA_INFO = {
  address: "Via Marittima, 59, Ercolano NA, Italy",
  email: "Info@sea-bnb.site",
  numberPhone: "+448172054550",
  numberPhoneFormat: "+44 (817) 205-45-50",
};

export const DATA_FOOTER_LINKS = [
  {
    name: "home",
    link: "",
  },
  {
    name: "rooms",
    link: "/rooms",
  },
  {
    name: "reservation",
    link: "/reservation",
  },
  {
    name: "service",
    link: "/services",
  },
  {
    name: "account",
    link: "/account",
  },
  {
    name: "restaurant",
    link: "/service/restaurant",
  },
  {
    name: "pool",
    link: "/service/pool",
  },
  {
    name: "spaSalon",
    link: "/service/spa",
  },
  {
    name: "activities",
    link: "/service/activities",
  },
  {
    name: "business",
    link: "/service/business",
  },
];

export const DATA_MENU: string[] = [
  "home",
  "rooms",
  "services",
  "about",
  "reservation",
  "account",
];
