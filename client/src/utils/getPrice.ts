import { EXCHANGE_RATES } from "../constants";

export const getPrice = (price: number, lang: string): number => {
  return price * EXCHANGE_RATES[lang.toLowerCase()];
};
