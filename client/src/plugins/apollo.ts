import { API_URL } from "../constants";
import ApolloClient from "apollo-boost";

export const clientApollo = new ApolloClient({
  uri: API_URL,
  request: (operation) => {
    const token = localStorage.getItem("token");
    operation.setContext({
      headers: {
        authorization: `Bearer ${token}`,
      },
    });
  },
});
