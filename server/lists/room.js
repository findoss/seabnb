const config = require("../constants");
const { Select, Relationship, Integer, Text } = require("@keystonejs/fields");

const TYPE_ROOM = [
  {
    value: "room-1",
    label: "Single",
  },
  {
    value: "room-2",
    label: "Family Room",
  },
  {
    value: "room-3",
    label: "Honeymoon suites",
  },
  {
    value: "room-4",
    label: "Lux single",
  },
  {
    value: "room-5",
    label: "Lux double",
  },
  {
    value: "room-6",
    label: "Premium single",
  },
  {
    value: "room-7",
    label: "Premium double",
  },
];

const scheme = {
  fields: {
    name: {
      type: Text,
      isRequired: true,
      isUnique: true,
    },
    type: {
      type: Select,
      options: TYPE_ROOM,
      dataType: "string",
      isRequired: true,
      defaultValue: TYPE_ROOM[0],
    },
    price: {
      type: Integer,
      isRequired: true,
    },
    count: {
      type: Integer,
      isRequired: true,
    },
    detal: {
      type: Relationship,
      ref: "RoomDetal",
      many: false,
      isRequired: true,
    },
    photo: {
      type: Relationship,
      ref: "Gallery",
      many: true,
      isRequired: true,
    },
  },
};

config.LANGS.forEach(lang => {
  scheme.fields[`${lang}_content`] = {
    type: Relationship,
    ref: "Content",
    many: false,
    isRequired: true,
  };
});

module.exports = scheme;
