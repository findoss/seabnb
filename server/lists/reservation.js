const {
  Virtual,
  Relationship,
  Integer,
  Text,
  Select,
} = require("@keystonejs/fields");
const { DateTimeUtc } = require("@keystonejs/fields-datetime-utc");
const {
  AuthedRelationship,
} = require("@keystonejs/fields-authed-relationship");
const reservationCreateHook = require("../hooks/reservationCreate");
const reservationUpdateStatusHook = require("../hooks/reservationUpdateStatus");
const isFreeRoomHook = require("../hooks/isFreeRoom");
const shortId = require("../utils/shortId");

const STATUS = ["pending", "approved", "rejected"];

module.exports = {
  fields: {
    shortId: {
      type: Virtual,
      resolver: item => `${shortId(item.id)}`,
    },
    dateIn: {
      type: DateTimeUtc,
      isRequired: true,
      adminConfig: {
        isReadOnly: true,
      },
    },
    dateOut: {
      type: DateTimeUtc,
      isRequired: true,
      adminConfig: {
        isReadOnly: true,
      },
    },
    room: {
      type: Relationship,
      ref: "Room",
      many: false,
      isRequired: true,
      adminConfig: {
        isReadOnly: true,
      },
    },
    customer: {
      type: Integer,
      isRequired: true,
      adminConfig: {
        isReadOnly: true,
      },
    },
    name: {
      type: Text,
      isRequired: true,
      adminConfig: {
        isReadOnly: true,
      },
    },
    user: {
      type: AuthedRelationship,
      ref: "User",
      isRequired: true,
      adminConfig: {
        isReadOnly: true,
      },
    },
    status: {
      type: Select,
      options: STATUS,
      dataType: "string",
      isRequired: true,
      defaultValue: STATUS[0],
      hooks: {
        resolveInput: async ({
          actions,
          resolvedData,
          operation,
          existingItem,
        }) => {
          const { status } = resolvedData;
          if (operation === "update") {
            return reservationUpdateStatusHook({
              actions,
              status,
              existingItem,
            });
          }
          return status;
        },
      },
    },
  },
  hooks: {
    resolveInput: async ({ actions, resolvedData, operation }) => {
      if (operation === "create") {
        return isFreeRoomHook({ actions, resolvedData });
      }
      return resolvedData;
    },
    afterChange: async ({ actions, updatedItem, operation }) => {
      if (operation === "create") {
        return reservationCreateHook({ actions, updatedItem });
      }
      return updatedItem;
    },
  },
};
