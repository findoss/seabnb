const config = require("../constants");
const { Select, Text } = require("@keystonejs/fields");
const { Markdown } = require("@keystonejs/fields-markdown");

module.exports = {
  fields: {
    name: {
      type: Text,
      isRequired: true,
      isUnique: true,
    },
    lang: {
      type: Select,
      options: config.LANGS,
      dataType: "string",
      isRequired: true,
      defaultValue: config.LANGS[0],
    },
    title: {
      type: Text,
    },
    subtitle: {
      type: Text,
    },
    description: {
      type: Markdown,
    },
    shortDescription: {
      type: Text,
    },
  },
};
