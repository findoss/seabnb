const config = require("../constants");
const { Checkbox, Relationship, Text } = require("@keystonejs/fields");

const scheme = {
  fields: {
    name: {
      type: Text,
      isRequired: true,
      isUnique: true,
    },
    isSelfPage: {
      type: Checkbox,
      defaultValue: false,
      isRequired: true,
    },
    photo: {
      type: Relationship,
      ref: "Gallery",
      many: true,
      isRequired: true,
    },
  },
};

config.LANGS.forEach(lang => {
  scheme.fields[`${lang}_content`] = {
    type: Relationship,
    ref: "Content",
    many: false,
    isRequired: true,
  };
});

module.exports = scheme;
