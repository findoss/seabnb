const config = require("../constants");
const { Relationship, Text } = require("@keystonejs/fields");

const scheme = {
  fields: {
    name: {
      type: Text,
      isRequired: true,
      isUnique: true,
    },
    photo: {
      type: Relationship,
      ref: "Gallery",
      many: true,
      isRequired: true,
    },
  },
};

config.LANGS.forEach(lang => {
  scheme.fields[`${lang}_content`] = {
    type: Relationship,
    ref: "Content",
    many: false,
    isRequired: true,
  };
});

module.exports = scheme;
