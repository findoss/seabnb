const { Text, Checkbox, Select } = require("@keystonejs/fields");

const BAD_TYPES = [
  {
    value: "bed-1",
    label: "Single",
  },
  {
    value: "bed-2",
    label: "Double",
  },
  {
    value: "bed-3",
    label: "Queen",
  },
  {
    value: "bed-4",
    label: "King",
  },
];

const VIEW_TYPES = [
  {
    value: "view-1",
    label: "Front sea view",
  },
  {
    value: "view-2",
    label: "Garden view",
  },
  {
    value: "view-3",
    label: "Land view",
  },
  {
    value: "view-4",
    label: "Mountain view",
  },
  {
    value: "view-5",
    label: "Sea view",
  },
];

module.exports = {
  fields: {
    name: {
      type: Text,
      isRequired: true,
      isUnique: true,
    },
    bed: {
      type: Select,
      options: BAD_TYPES,
      dataType: "string",
      defaultValue: BAD_TYPES[0],
    },
    occupancy: {
      type: Text,
    },
    size: {
      type: Text,
    },
    wifi: {
      type: Checkbox,
    },
    bathroom: {
      type: Checkbox,
    },
    isBreakfast: {
      type: Checkbox,
    },
    gymAccess: {
      type: Text,
    },
    airportPickup: {
      type: Checkbox,
    },
    roomSevice: {
      type: Checkbox,
    },
    view: {
      type: Select,
      options: VIEW_TYPES,
      dataType: "string",
      defaultValue: VIEW_TYPES[0],
    },
  },
};
