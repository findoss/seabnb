const { Url, Text, Checkbox } = require("@keystonejs/fields");
const { DateTimeUtc } = require("@keystonejs/fields-datetime-utc");
const {
  AuthedRelationship,
} = require("@keystonejs/fields-authed-relationship");

module.exports = {
  fields: {
    avatar: {
      type: Url,
      defaultValue:
        "https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png",
    },
    user: {
      type: AuthedRelationship,
      ref: "User",
      isRequired: true,
    },
    text: {
      type: Text,
      isRequired: true,
    },
    isApprove: {
      type: Checkbox,
    },
    date: {
      type: DateTimeUtc,
      defaultValue: () => new Date(),
    },
  },
};
