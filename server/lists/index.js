const review = require("./review");
const user = require("./user");
const room = require("./room");
const history = require("./history");
const gallery = require("./gallery");
const roomDetal = require("./roomDetal");
const content = require("./content");
const service = require("./service");
const reservation = require("./reservation");
const email = require("./email");
const block = require("./block");

module.exports = keystone => {
  keystone.createList("User", user);
  keystone.createList("Content", content);
  keystone.createList("Gallery", gallery);
  keystone.createList("Room", room);
  keystone.createList("RoomDetal", roomDetal);
  keystone.createList("Review", review);
  keystone.createList("Reservation", reservation);
  keystone.createList("History", history);
  keystone.createList("Service", service);
  keystone.createList("Email", email);
  keystone.createList("Block", block);
};
