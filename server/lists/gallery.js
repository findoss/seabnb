const config = require("../constants");
const { Text, File } = require("@keystonejs/fields");
const { LocalFileAdapter } = require("@keystonejs/file-adapters");

const src = config.IS_PROD
  ? "./dist/client/build/files"
  : "../client/public/files";

const localImageFiles = new LocalFileAdapter({
  src: src,
  path: "/files",
});

module.exports = {
  fields: {
    name: {
      type: Text,
      isRequired: true,
      isUnique: true,
    },
    file: {
      type: File,
      adapter: localImageFiles,
      isRequired: true,
    },
  },
};
