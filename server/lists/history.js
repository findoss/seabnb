const config = require("../constants");
const { Text, Relationship } = require("@keystonejs/fields");
const { DateTimeUtc } = require("@keystonejs/fields-datetime-utc");

const scheme = {
  fields: {
    name: {
      type: Text,
      isRequired: true,
      isUnique: true,
    },
    date: {
      type: DateTimeUtc,
      isRequired: true,
    },
  },
};

config.LANGS.forEach(lang => {
  scheme.fields[`${lang}_content`] = {
    type: Relationship,
    ref: "Content",
    many: false,
    isRequired: true,
  };
});

module.exports = scheme;
