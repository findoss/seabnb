const { Text, Password, Checkbox } = require("@keystonejs/fields");
const registrationHook = require("../hooks/registration");
const resetPasswordHook = require("../hooks/resetPassword");
const genPassword = require("../utils/genPassword");

module.exports = {
  fields: {
    name: {
      type: Text,
      isRequired: true,
    },
    email: {
      type: Text,
      isUnique: true,
      isRequired: true,
    },
    password: {
      type: Password,
      defaultValue: () => genPassword(),
      hooks: {
        resolveInput: async ({ resolvedData, existingItem, operation }) => {
          if (operation === "update") {
            if (resolvedData.password === "reset_password") {
              return resetPasswordHook({ existingItem });
            }
          }
          return resolvedData.password;
        },
      },
    },
    isAdmin: {
      type: Checkbox,
      defaultValue: false,
    },
  },
  hooks: {
    resolveInput: async ({ resolvedData, operation }) => {
      if (operation === "create") {
        return registrationHook({ resolvedData });
      }
      return resolvedData;
    },
  },
};

// hooks: {
//   // Hooks for create and update operations
//   resolveInput: async (...) => {...},
//   validateInput: async (...) => {...},
//   beforeChange: async (...) => {...},
//   afterChange: async (...) => {...},

//   // Hooks for delete operations
//   validateDelete: async (...) => {...},
//   beforeDelete: async (...) => {...},
//   afterDelete: async (...) => {...},
// },
