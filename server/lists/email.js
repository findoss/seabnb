const { Relationship, Text } = require("@keystonejs/fields");
const sendEmailHook = require("../hooks/sendEmail");

const scheme = {
  fields: {
    name: {
      type: Text,
      isRequired: true,
    },
    from: {
      type: Relationship,
      ref: "User",
      many: true,
      isRequired: true,
    },
    subject: {
      type: Text,
    },
    text: {
      type: Text,
    },
  },
  hooks: {
    resolveInput: async ({ actions, resolvedData }) =>
      sendEmailHook({ actions, resolvedData }),
  },
};

module.exports = scheme;
