const config = require("../constants");

module.exports = {
  User: [
    {
      name: "Admin",
      email: config.ADMIN.name,
      password: config.ADMIN.pass,
      isAdmin: true
    },
    {
      name: "Tester",
      email: "test@test.com",
      password: "qwerqwer"
    },
    {
      name: "Anonymous",
      email: "anonymous@test.com",
      password: "qwerqwer"
    }
  ]
};
