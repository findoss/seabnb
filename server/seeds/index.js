const config = require("../constants");

const users = require("./users");

module.exports = async keystone => {
  if (config.SEEDS) {
    const {
      data: {
        _allUsersMeta: { count },
      },
    } = await keystone.executeQuery(`query { _allUsersMeta { count } }`);

    if (count === 0) {
      try {
        await keystone.createItems({
          ...users,
        });
      } catch (error) {
        console.error(error);
      }
    }
  }
};
