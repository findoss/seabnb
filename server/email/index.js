const { emailSender } = require("@keystonejs/email");

module.exports = emailSender.jsx({
  root: `${__dirname}/`,
  transport: "nodemailer",
});

// new Email('template.pug').render(locals, (err, { html, text }) => { /* rendered */ })
