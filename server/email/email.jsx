import React from "react";
import { Page } from "./UI/body";

module.exports = ({ name, text }) => {
  return (
    <Page>
      <div>
        <h1>Hello {name}!</h1>
        <div>{String(text)}</div>
      </div>
    </Page>
  );
};
