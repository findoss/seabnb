import React from "react";
import { Page } from "./UI/body";

module.exports = ({ name, newPassword }) => {
  return (
    <Page>
      <div>
        <div>Hello {name}</div>
        <br />
        <div>
          New password:
          <b
            style={{
              margin: "5px",
              padding: "5px",
              border: "1px solid black",
            }}
          >
            {newPassword}
          </b>
        </div>
      </div>
    </Page>
  );
};
