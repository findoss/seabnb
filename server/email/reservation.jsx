import React from "react";
import { Page } from "./UI/body";

module.exports = ({
  title,
  description,
  dateIn,
  dateOut,
  customer,
  name,
  codeReservation,
  status,
}) => {
  console.log({
    title,
    description,
    dateIn,
    dateOut,
    customer,
    name,
    codeReservation,
    status,
  });

  return (
    <Page>
      <div>
        <h1>Hello {name}!</h1>
        <h2>Code reservation {codeReservation}</h2>
        <h3>Status {status}</h3>
        <br />
        <table cellPadding="5" width="90%">
          <tr>
            <td>Room</td>
            <td>{title}</td>
          </tr>
          <tr>
            <td colSpan="2">{String(description)}</td>
          </tr>
          <tr>
            <td>Date in</td>
            <td>{String(dateIn)}</td>
          </tr>
          <tr>
            <td>Date out</td>
            <td>{String(dateOut)}</td>
          </tr>
          <tr>
            <td>Customer</td>
            <td>{customer}</td>
          </tr>
        </table>
      </div>
    </Page>
  );
};
