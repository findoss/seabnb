import React from "react";
import { Page } from "./UI/body";

module.exports = ({ name, password }) => {
  return (
    <Page>
      <div>
        <div>Hello {name}</div>
        <div>pass: {password}</div>
      </div>
    </Page>
  );
};
