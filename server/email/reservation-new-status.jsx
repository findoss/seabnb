import React from "react";
import { Page } from "./UI/body";

module.exports = ({ name, newStatus, codeReservation }) => {
  return (
    <Page>
      <div>
        <h1>Hello {name}!</h1>
        <h2>Code reservation {codeReservation}</h2>
        <h3>Status {newStatus}</h3>
      </div>
    </Page>
  );
};
