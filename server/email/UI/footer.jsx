import React from "react";

export const Footer = () => {
  return (
    <div>
      <div>Hotel manager - Sea B&amp;B</div>
      <div>Via Marittima, 59, Ercolano NA, Italy</div>
      <div>
        <a href="tel:+448172054550">+44 (817) 205-45-50</a>
      </div>
    </div>
  );
};
