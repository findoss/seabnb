import React from "react";
import { Footer } from "./footer";
import { Header } from "./header";

export const Page = ({ children }) => {
  return (
    <html>
      <body>
        <Header />
        <div>{children}</div>
        <br />
        <hr />
        <Footer />
      </body>
    </html>
  );
};
