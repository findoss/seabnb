const config = require("../constants");
const { GraphQLApp } = require("@keystonejs/app-graphql");
const { AdminUIApp } = require("@keystonejs/app-admin-ui");
const { StaticApp } = require("@keystonejs/app-static");

const { PasswordAuthStrategy } = require("@keystonejs/auth-password");

module.exports = keystone => {
  const authStrategy = keystone.createAuthStrategy({
    type: PasswordAuthStrategy,
    list: "User",
  });

  const authAccessAllowed = ({
    authentication: { item: user, listKey: list },
  }) => !!user && !!user.isAdmin;

  const srcStatic = config.IS_PROD ? "./client/build" : "../client/public";
  return [
    new GraphQLApp(),
    new StaticApp({ path: "/", src: srcStatic }),
    new AdminUIApp({
      authStrategy,
      enableDefaultRoute: true,
      hooks: require.resolve("../hooks/hooks-admin"),
      isAccessAllowed: authAccessAllowed,
    }),
  ];
};
