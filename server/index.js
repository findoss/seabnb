const { Keystone } = require("@keystonejs/keystone");
const { MongooseAdapter } = require("@keystonejs/adapter-mongoose");

const config = require("./constants");

const apps = require("./apps");
const seeds = require("./seeds");
const lists = require("./lists");

const keystone = new Keystone({
  name: config.PROJECT_NAME,
  adapter: new MongooseAdapter({ mongoUri: config.MONGO_URI }),
  cookieSecret: config.COOKIE_SECRET,
  cookie: {
    secure: false,
    maxAge: 1000 * 60 * 60 * 24 * 30,
    sameSite: false,
  },
  onConnect: async keystone => {
    await seeds(keystone);
  },
});

lists(keystone);

module.exports = {
  keystone,
  apps: apps(keystone),
};
