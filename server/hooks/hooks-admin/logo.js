import React from "react";
import logoIcon from "./logo.svg";

export const logo = () => <img style={{ width: "200px" }} src={logoIcon} />;
