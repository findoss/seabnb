const config = require("../constants");
const sendEmail = require("../email");
const genPassword = require("../utils/genPassword");

module.exports = async ({ existingItem }) => {
  const { email, name } = existingItem;
  const newPassword = genPassword();

  async function send(configEmail) {
    return sendEmail("new-password.jsx").send(
      { name, newPassword },
      {
        to: email,
        subject: "New password",
        from: {
          name: config.PROJECT_NAME,
          address: configEmail.auth.user,
        },
        nodemailerConfig: configEmail,
      }
    );
  }

  try {
    if (config.IS_PROD || config.SEND_EMAIL) {
      await send(config.EMAIL);
    }
  } catch (error) {
    console.error(error);
    try {
      console.info("retry");
      await send(config.EMAIL_RESERVE);
    } catch (error) {
      console.error(error);
      try {
        console.info("retry 2");
        await send(config.EMAIL_RESERVE2);
      } catch (error) {
        console.error(error);
        throw new Error("Email not send!");
      }
    }
  }
  return newPassword;
};
