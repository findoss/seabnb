const config = require("../constants");
const sendEmail = require("../email");

module.exports = async ({ resolvedData }) => {
  const { email, name, password } = resolvedData;
  console.log("NEW USER", { email, name, password });

  try {
    if (config.IS_PROD || config.SEND_EMAIL) {
      await sendEmail("new-user.jsx").send(
        { name, password },
        {
          to: email,
          subject: "Registration",
          from: {
            name: config.PROJECT_NAME,
            address: config.EMAIL.auth.user,
          },
          nodemailerConfig: config.EMAIL,
        }
      );
    }
  } catch (error) {
    console.error(error);
    throw new Error("Email not send!");
  }
  return resolvedData;
};
