const shortId = require("../utils/shortId");
const config = require("../constants");
const sendEmail = require("../email");

module.exports = async ({ updatedItem, actions }) => {
  const { query } = actions;
  const { room, user, id } = updatedItem;
  const codeReservation = shortId(id);

  const queryRoom = `
    query getRoom($roomId: ID!, $userId: ID!) {
      Room(
        where: { id: $roomId }
      ) {
        en_content {
          title
          description
        }
      }
      User(
        where: { id: $userId }
      ) {
        email
      }
    }
  `;

  const {
    data: { Room, User },
  } = await query(queryRoom, {
    skipAccessControl: false,
    variables: {
      roomId: String(room),
      userId: String(user),
    },
  });

  const roomData = Room[`${config.LANGS[0]}_content`];

  try {
    if (config.IS_PROD || config.SEND_EMAIL) {
      await sendEmail("reservation.jsx").send(
        { ...updatedItem, ...roomData, codeReservation },
        {
          to: User.email,
          subject: `Reservation [${codeReservation}]`,
          from: {
            name: config.PROJECT_NAME,
            address: config.EMAIL.auth.user,
          },
          nodemailerConfig: config.EMAIL,
        }
      );
    }
  } catch (error) {
    console.error(error);
    throw new Error("Email not send!");
  }

  return updatedItem;
};
