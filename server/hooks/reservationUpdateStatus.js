const shortId = require("../utils/shortId");
const config = require("../constants");
const sendEmail = require("../email");

module.exports = async ({ status, actions, existingItem }) => {
  const { query } = actions;
  const { id, user } = existingItem;
  const codeReservation = shortId(id);

  const queryUserEmail = `
    query getEmail($id: ID!) {
      User(
        where: { id: $id }
      ) {
        email
      }
    }
  `;
  const {
    data: { User },
  } = await query(queryUserEmail, {
    skipAccessControl: false,
    variables: {
      id: String(user),
    },
  });

  try {
    if (config.IS_PROD || config.SEND_EMAIL) {
      await sendEmail("reservation-new-status.jsx").send(
        { ...existingItem, codeReservation, newStatus: status },
        {
          to: User.email,
          subject: `Update status reservation [${codeReservation}]`,
          from: {
            name: config.PROJECT_NAME,
            address: config.EMAIL.auth.user,
          },
          nodemailerConfig: config.EMAIL,
        }
      );
    }
  } catch (error) {
    console.error(error);
    throw new Error("Email not send!");
  }
  return status;
};
