// const config = require("../constants");

module.exports = async ({ actions, resolvedData }) => {
  const { dateIn, dateOut, room } = resolvedData;
  const { query } = actions;

  // N - new reservation range
  // B - current reservation ranges
  //
  // variant 1
  //      N-----N
  //   B-----B
  //
  //  variant 2
  //  N-----N
  //     B-----B
  //
  //  variant 3
  //  N---------N
  //     B---B
  //
  const queryFreeRoom = `
    query checkFreeRoom($id: ID!, $in: String, $out: String) {
      allReservations(
        where: {
          AND: [
            { room: { id: $id } }
            {
              OR: [
                { dateIn_lt: $in, dateOut_gt: $in }
                { dateIn_gt: $out, dateOut_lt: $out }
                { dateIn_gt: $in, dateOut_lt: $out }
              ]
            }
          ]
        }
      ) {
        id
      }
      Room(where: {id: $id}) {
        count
      }
    }
  `;

  const {
    data: { allReservations, Room },
  } = await query(queryFreeRoom, {
    skipAccessControl: false,
    variables: {
      id: room,
      in: dateIn,
      out: dateOut,
    },
  });

  const countReservationRoom = allReservations.length;
  const countRoom = Room.count;

  if (countReservationRoom + 1 > countRoom) {
    throw new Error("On selected dates the hotel room is busy.");
  }

  return resolvedData;
};
