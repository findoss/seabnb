const config = require("../constants");
const sendEmail = require("../email");

module.exports = async ({ actions, resolvedData }) => {
  const { name, text, subject, from } = resolvedData;
  const { query } = actions;

  const queryUsersEmail = `
    query getEmails($from: [ID]) {
      allUsers(
        where: { id_in: $from }
      ) {
        email
      }
    } 
  `;

  const {
    data: { allUsers },
  } = await query(queryUsersEmail, {
    skipAccessControl: false,
    variables: {
      from: from,
    },
  });
  const toEmails = allUsers.map(v => v.email).toString();

  async function send(configEmail) {
    return sendEmail("email.jsx").send(
      { name, text },
      {
        to: toEmails,
        subject: subject,
        from: {
          name: config.PROJECT_NAME,
          address: configEmail.auth.user,
        },
        nodemailerConfig: configEmail,
      }
    );
  }

  try {
    if (config.IS_PROD || config.SEND_EMAIL) {
      await send(config.EMAIL);
    }
  } catch (error) {
    console.error(error);
    try {
      console.info("retry");
      await send(config.EMAIL_RESERVE);
    } catch (error) {
      console.error(error);
      try {
        console.info("retry 2");
        await send(config.EMAIL_RESERVE2);
      } catch (error) {
        console.error(error);
        throw new Error("Email not send!");
      }
    }
  }

  return resolvedData;
};
