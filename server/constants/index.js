module.exports = {
  PROJECT_NAME: "Hotel Sea B&B",
  MONGO_URI:
    process.env.NODE_ENV === "development"
      ? "mongodb://root:example@localhost"
      : "mongodb://root:example@mongo",
  ADMIN: {
    name: "Admin",
    pass: "qwerqwer",
  },
  EMAIL: {
    host: "smtp.yandex.ru",
    port: 465,
    secure: true,
    auth: {
      user: "info@sea-bnb.site",
      pass: "qwerqwer",
    },
  },
  EMAIL_RESERVE: {
    host: "smtp.yandex.ru",
    port: 465,
    secure: true,
    auth: {
      user: "info-reserve@sea-bnb.site",
      pass: "qwerqwer",
    },
  },
  EMAIL_RESERVE2: {
    host: "smtp.yandex.ru",
    port: 465,
    secure: true,
    auth: {
      user: "aengorg@yandex.ru",
      pass: "qwerqwer",
    },
  },
  SEEDS: true,
  COOKIE_SECRET: "s6f5s65g6sd51b6xf5g4h6a5e1g6b1vbn5k,.4y6u51jd54fg85d",
  LANGS: ["en", "ru"],
  SEND_EMAIL: true,
  IS_PROD: process.env.NODE_ENV === "production",
  IS_DEV: process.env.NODE_ENV === "development",
};
