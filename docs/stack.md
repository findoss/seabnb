# Stack

## Langs

- JavaScript
- TypeScript

* HTML
* CSS
* SCSS

## Frontend (SPA)

- React
- Apollo GraphQL
- i18n

## DataBase

- MongoDB

## Backend (GraphQL API)

- Node.js
- Express
- Mongoose
- Keystone
- GraphQL

## Host

- Ubuntu 18
- Docker
- Docker-compose

## Other

- Auth JWT
- HTTP
- JSON
- GIT
- SSH
- SSL
