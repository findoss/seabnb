# Quick start

## Install project - dev?

Version lock

```
git: v2+
docker: v19+
docker-compose: v1.25.5+
node: v12
yarn: v1.22+
```

0. install git
1. install docker
2. install docker-compose
3. install node
4. install yarn
5. `docker-compose -f stack.dev.yml up`
6. `cd client` + `yarn install`
7. `cd server` + `yarn install`

## Start project dev?

0. `docker-compose -f stack.dev.yml up`
1. `cd client` + `yarn dev`
2. `cd server` + `yarn dev`

## Install and Start project - prod?

0. install git
1. install docker
2. install docker-compose
3. `cd /home`
4. `git clone --branch master https://github.com/Findoss/Sea-bnb.git`
5. `cd Sea-bnb`
6. `docker-compose -f stack.yml up --build -d`

## Update project - prod?

0. `cd /home/Sea-bnb`
1. `git pull`
2. `docker-compose -f stack.yml up --build -d`
   ...

## How install Docker Ubuntu-18?

```sh
sudo apt-get update
```

```sh
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
```

```sh
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

```sh
sudo apt-key fingerprint 0EBFCD88
```

```sh
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```

```sh
sudo apt-get update
```

```sh
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

```sh
docker -v
```

## How install Docker-compose Ubuntu-18?

```sh
sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
```

```sh
sudo chmod +x /usr/local/bin/docker-compose
```

```sh
docker-compose --version
```
