# Host

`185.43.6.147`  
`sea-bnb.site`

# Admin

## Admin Repo GitHub

**nope**  
send you email to me and i add access

## Admin Host

**nope**

## Admin CloudFlare

**nope**

## Admin Domain

**nope**

## Admin DataBase

- [Admin UI - localhost](http://localhost:8081)
- [Admin UI - prod](http://185.43.6.147:8081)

| user | pass      |
| ---- | --------- |
| root | `example` |

## Admin Sea B&B

- [Admin UI - localhost](http://localhost:3000/admin)
- [Admin UI - prod](https://sea-bnb.site/admin)

| user               | pass           |
| ------------------ | -------------- |
| Admin              | `qwerqwer`     |
| anonymous@test.com | `qwerqwer`     |
| test@test.com      | `qwerqwer`     |
| test1@test.com     | `29af27e2d3d2` |
| test2@test.com     | `ffb2c424381c` |
| test3@test.com     | `a8005de74176` |
| test4@test.com     | `8010dccb78a2` |
| test5@test.com     | `a62903610ff8` |

## Yandex admin

- [Yandex mail](https://yandex.ru)
- [Yandex metrika](https://metrika.yandex.ru/dashboard?id=64441444)

| user    | pass               |
| ------- | ------------------ |
| aengorg | `8MGj2wDH29P2jAxc` |

## Yandex mail users

- [Yandex mail bisness](https://admin.yandex.ru/mail)

| user                      | pass mail  | pass site | mailing |
| ------------------------- | ---------- | --------- | ------- |
| info@sea-bnb.site         | `qwerqwer` |           | TRUE    |
| info-reserve@sea-bnb.site | `qwerqwer` |           | TRUE    |
| admin@sea-bnb.site        | `qwerqwer` |           |         |
| moderator@sea-bnb.site    | `qwerqwer` |           |         |
| test@sea-bnb.site         | `qwerqwer` |           |         |
| help@sea-bnb.site         | `qwerqwer` |           |         |
