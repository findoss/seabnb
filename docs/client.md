# Available Scripts

In the project directory, you can run:

## `yarn dev`

Runs the app in the development mode.<br />
Open [http://localhost:5888](http://localhost:5888) to view it in the browser.

## `yarn test`

Launches the test runner in the interactive watch mode.

## `yarn build`

Builds the app for production to the `build` folder.<br />

```
.
├── package.json
├── public
│   ├── favicon.ico
│   ├── files
│   │   ├── ...
│   ├── index.html
│   ├── locales
│   │   ├── en
│   │   │   └── translation.json
│   │   └── ru
│   │       └── translation.json
│   ├── logo192.png
│   ├── logo512.png
│   ├── logo-email.png
│   ├── logo-invert.svg
│   ├── logo.svg
│   ├── manifest.json
│   └── robots.txt
├── src
│   ├── api
│   │   ├── login.ts
│   │   └── registration.ts
│   ├── App.scss
│   ├── App.tsx
│   ├── assets
│   │   ├── fonts
│   │   ├── icons
│   │   │   ├── account.svg
│   │   │   ├── add.svg
│   │   │   ├── arrow_down.svg
│   │   │   ├── chat.svg
│   │   │   ├── details
│   │   │   │   ├── air-conditioner.svg
│   │   │   │   ├── bar.svg
│   │   │   │   ├── bathtub.svg
│   │   │   │   ├── hair-dryer.svg
│   │   │   │   ├── room-service.svg
│   │   │   │   └── tv.svg
│   │   │   ├── error.svg
│   │   │   ├── exit.svg
│   │   │   ├── loading.svg
│   │   │   ├── logo.svg
│   │   │   ├── mail.svg
│   │   │   ├── menu.svg
│   │   │   ├── phone.svg
│   │   │   ├── place.svg
│   │   │   ├── search.svg
│   │   │   ├── send.svg
│   │   │   └── social
│   │   │       ├── facebook.svg
│   │   │       ├── instagram.svg
│   │   │       ├── skype.svg
│   │   │       └── twitter.svg
│   │   └── images
│   │       ├── bg-home.png
│   │       ├── logo.svg
│   │       └── map.png
│   ├── components
│   │   ├── ErrorForm
│   │   │   ├── ErrorForm.scss
│   │   │   ├── ErrorForm.tsx
│   │   │   ├── index.ts
│   │   │   └── types.ts
│   │   ├── Footer
│   │   │   ├── Footer.scss
│   │   │   ├── Footer.tsx
│   │   │   ├── index.ts
│   │   │   └── types.ts
│   │   ├── Header
│   │   │   ├── Header.scss
│   │   │   ├── Header.tsx
│   │   │   ├── index.ts
│   │   │   └── types.ts
│   │   ├── Menu
│   │   │   ├── index.ts
│   │   │   ├── Menu.scss
│   │   │   ├── Menu.tsx
│   │   │   └── types.ts
│   │   ├── NavForm
│   │   │   ├── index.ts
│   │   │   ├── NavForm.scss
│   │   │   ├── NavForm.tsx
│   │   │   └── types.ts
│   │   ├── ReviewArea
│   │   │   ├── index.ts
│   │   │   ├── ReviewArea.scss
│   │   │   ├── ReviewArea.tsx
│   │   │   └── types.ts
│   │   └── UI
│   │       ├── Button
│   │       │   ├── Button.scss
│   │       │   ├── Button.tsx
│   │       │   ├── index.ts
│   │       │   └── types.ts
│   │       ├── ScrollToTop
│   │       │   ├── index.ts
│   │       │   ├── ScrollToTop.scss
│   │       │   ├── ScrollToTop.tsx
│   │       │   └── types.ts
│   │       ├── TextField
│   │       │   ├── index.ts
│   │       │   ├── TextField.scss
│   │       │   ├── TextField.tsx
│   │       │   └── types.ts
│   │       └── Title
│   │           ├── index.ts
│   │           ├── Title.scss
│   │           ├── Title.tsx
│   │           └── types.ts
│   ├── constants
│   │   └── index.ts
│   ├── HOCs
│   │   └── logger.tsx
│   ├── index.tsx
│   ├── plugins
│   │   ├── apollo.ts
│   │   ├── context.ts
│   │   └── i18n.ts
│   ├── react-app-env.d.ts
│   ├── serviceWorker.ts
│   ├── setupTests.ts
│   ├── styles
│   │   ├── index.css
│   │   └── typography.css
│   ├── types.d.ts
│   ├── utils
│   │   ├── format.ts
│   │   ├── getPrice.ts
│   │   ├── http.ts
│   │   └── validationEmail.ts
│   └── views
│       ├── About
│       │   ├── About.scss
│       │   ├── About.tsx
│       │   ├── index.ts
│       │   └── types.ts
│       ├── Account
│       │   ├── Account.scss
│       │   ├── Account.tsx
│       │   ├── index.ts
│       │   └── types.ts
│       ├── ErrorPage
│       │   ├── ErrorPage.scss
│       │   ├── ErrorPage.tsx
│       │   ├── index.ts
│       │   └── types.ts
│       ├── Home
│       │   ├── Home.scss
│       │   ├── Home.tsx
│       │   ├── index.ts
│       │   └── types.ts
│       ├── Login
│       │   ├── index.ts
│       │   ├── Login.scss
│       │   ├── Login.tsx
│       │   └── types.ts
│       ├── Registration
│       │   ├── index.ts
│       │   ├── Registration.scss
│       │   ├── Registration.tsx
│       │   └── types.ts
│       ├── Reservation
│       │   ├── index.ts
│       │   ├── Reservation.scss
│       │   ├── Reservation.tsx
│       │   └── types.ts
│       ├── ResetPass
│       │   ├── index.ts
│       │   ├── ResetPass.scss
│       │   ├── ResetPass.tsx
│       │   └── types.ts
│       ├── Room
│       │   ├── index.ts
│       │   ├── Room.scss
│       │   ├── Room.tsx
│       │   └── types.ts
│       ├── Rooms
│       │   ├── index.ts
│       │   ├── Rooms.scss
│       │   ├── Rooms.tsx
│       │   └── types.ts
│       ├── Service
│       │   ├── index.ts
│       │   ├── Service.scss
│       │   ├── Service.tsx
│       │   └── types.ts
│       └── Services
│           ├── index.ts
│           ├── Services.scss
│           ├── Services.tsx
│           └── types.ts
├── tsconfig.json
└── yarn.lock
```
