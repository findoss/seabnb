# Available Scripts

In the project directory, you can run:

## `yarn dev`

Runs the app in the development mode.<br />
Open [http://localhost:3000/admin](http://localhost:3000/admin) to view it in the browser.

## `yarn build`

Builds the app for production to the `build` folder.<br />

## `yarn start`

...

`tree -I 'node_modules|dist|bundle|build|files|cache|test_*'`

```
.
├── apps
│   └── index.js
├── constants
│   └── index.js
├── email
│   ├── email.jsx
│   ├── index.js
│   ├── new-password.jsx
│   ├── new-user.jsx
│   ├── reservation.jsx
│   ├── reservation-new-status.jsx
│   └── UI
│       ├── body.jsx
│       ├── footer.jsx
│       └── header.jsx
├── hooks
│   ├── hooks-admin
│   │   ├── index.js
│   │   ├── logo.js
│   │   └── logo.svg
│   ├── isFreeRoom.js
│   ├── registration.js
│   ├── reservationCreate.js
│   ├── reservationUpdateStatus.js
│   ├── resetPassword.js
│   └── sendEmail.js
├── index.js
├── lists
│   ├── block.js
│   ├── content.js
│   ├── email.js
│   ├── gallery.js
│   ├── history.js
│   ├── index.js
│   ├── reservation.js
│   ├── review.js
│   ├── roomDetal.js
│   ├── room.js
│   ├── service.js
│   └── user.js
├── package.json
├── patches
│   └── keystone-email+1.1.0.patch
├── seeds
│   ├── index.js
│   └── users.js
├── utils
│   ├── genPassword.js
│   ├── libs
│   └── shortId.js
└── yarn.lock
```
